/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    int anzahlSterne = 400 ;
    
    // Wie viele Einwohner hat Berlin?
    int    bewohnerBerlin = 3769 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 8760 ; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm = 190000 ;  
    
    // Schreiben Sie auf, wie viele km² das größte Land der Erde hat?
    int   flaecheGroessteLand = 17098242 ; 
    
    // Wie groß ist das kleinste Land der Erde?
    
    double   flaecheKleinsteLand = 0.44 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Einwoner in Berlin: "+ bewohnerBerlin);
    System.out.println("Alter in Tagen: " + alterTage);
    System.out.println("Das schwerste Tier in Kilogram: " + gewichtKilogramm);
    System.out.println("Das gro�te Land der Erde: " + flaecheGroessteLand);
    System.out.println("Das kleinste Land der Erde: " + flaecheKleinsteLand);
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

